<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Personas;

class PersonasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            return Personas::get();
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        try {
            $guardar = new Personas;
            $guardar->nombre = $request->nombre;
            $guardar->apellido = $request->apellido;
            $guardar->telofono = $request->telofono;
            $guardar->correo = $request->correo;
            $guardar->direccion = $request->direccion;
            $guardar->save();

        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return Personas::where('id',$id)->get();
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $editar = Personas::find($id);
            $editar->nombre = $request->nombre;
            $editar->apellido = $request->apellido;
            $editar->telofono = $request->telofono;
            $editar->correo = $request->correo;
            $editar->direccion = $request->direccion;
            $editar->save();
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
           $eliminar = Personas::find($id);
           $eliminar->delete();
           return ['mensaje'=>'Eliminado correctamente'];
        } catch (\Throwable $th) {
            //throw $th;
        }
    }
}
